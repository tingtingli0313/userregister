﻿namespace DataAccess
{
    public class ProfileImage
    {
        public int ProfileImageId { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ImageUrl { get; set; }

        public int UserRef { get; set; }
        public User User { get; set; } 
    }
}