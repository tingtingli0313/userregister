﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
       
        }
        public DbSet<User> Users { get; set; }
        public DbSet<ProfileImage> ProfileImages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
               .HasKey(x => x.UserId);

            modelBuilder.Entity<User>()
                       .HasOne(u => u.ProfileImage)
                       .WithOne(p => p.User)
                       .HasForeignKey<ProfileImage>(p => p.UserRef);
            //seed categories
         

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test1",
                LastName = "John",
                UserId = 1,
                Title = "Mr",
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test2",
                LastName = "John",
                UserId = 2,
                Title = "Mrs",
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test3",
                LastName = "John",
                UserId = 3,
                Title = "Mr"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test4",
                LastName = "Simpson",
                UserId = 4,
                Title = "Miss"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test5",
                LastName = "Simpson",
                UserId = 5,
                Title = "Miss"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test6",
                LastName = "Simpson",
                UserId = 6,
                Title = "Miss",
                //  ProfileImageId = 3,
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test7",
                LastName = "Smith",
                UserId = 7,
                Title = ""
            });


            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test8",
                LastName = "Joe",
                UserId = 8,
                Title = ""
            });


            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test9",
                LastName = "Joe",
                UserId = 9,
                Title = "Mr"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test10",
                LastName = "Joe",
                UserId = 10,
                Title = "Mrs"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test10",
                LastName = "Joe",
                UserId = 11,
                Title = "Mr"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                FirstName = "test11",
                LastName = "John",
                UserId = 12,
                Title = "Mr"
            });

            modelBuilder.Entity<ProfileImage>().HasData(new ProfileImage
            {
                ProfileImageId = 1,
                UserRef = 1,
                ImageUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/applepie.jpg",
                ThumbnailUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/applepiesmall.jpg"
            });

            modelBuilder.Entity<ProfileImage>().HasData(new ProfileImage
            {
                ProfileImageId = 2,
                UserRef = 2,
                ImageUrl = "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjot8r-3OPmAhUQVH0KHalYB1M4ZBAzKEgwSHoECAEQTw&url=https%3A%2F%2Fwww.whatsup.co.nz%2Fkids%2F&psig=AOvVaw1DgLCoMn5PlxvsLoBNNkBn&ust=1578013626069260&ictx=3&uact=3",
                ThumbnailUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJU4OPVn48JeOo2gx4_S2EVQH4jJ3qMi6-opxYhXRan28Gpk6T&s"
            });

            modelBuilder.Entity<ProfileImage>().HasData(new ProfileImage
            {
                ProfileImageId = 3,
                UserRef = 3,
                ImageUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/peachpie.jpg",
                ThumbnailUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/peachpiesmall.jpg",
            });

        }
    }
}
