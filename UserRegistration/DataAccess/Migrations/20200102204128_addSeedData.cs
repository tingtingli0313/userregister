﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class addSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "DOB", "FirstName", "LastName", "PhoneNo", "Title" },
                values: new object[,]
                {
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test1", "John", null, "Mr" },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test2", "John", null, "Mrs" },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test3", "John", null, "Mr" },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test4", "Simpson", null, "Miss" },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test5", "Simpson", null, "Miss" },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test6", "Simpson", null, "Miss" },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test7", "Smith", null, "" },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test8", "Joe", null, "" },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test9", "Joe", null, "Mr" },
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test10", "Joe", null, "Mrs" },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test10", "Joe", null, "Mr" },
                    { 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test11", "John", null, "Mr" }
                });

            migrationBuilder.InsertData(
                table: "ProfileImages",
                columns: new[] { "ProfileImageId", "ImageUrl", "ThumbnailUrl", "UserRef" },
                values: new object[] { 1, "https://gillcleerenpluralsight.blob.core.windows.net/files/applepie.jpg", "https://gillcleerenpluralsight.blob.core.windows.net/files/applepiesmall.jpg", 1 });

            migrationBuilder.InsertData(
                table: "ProfileImages",
                columns: new[] { "ProfileImageId", "ImageUrl", "ThumbnailUrl", "UserRef" },
                values: new object[] { 2, "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjot8r-3OPmAhUQVH0KHalYB1M4ZBAzKEgwSHoECAEQTw&url=https%3A%2F%2Fwww.whatsup.co.nz%2Fkids%2F&psig=AOvVaw1DgLCoMn5PlxvsLoBNNkBn&ust=1578013626069260&ictx=3&uact=3", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJU4OPVn48JeOo2gx4_S2EVQH4jJ3qMi6-opxYhXRan28Gpk6T&s", 2 });

            migrationBuilder.InsertData(
                table: "ProfileImages",
                columns: new[] { "ProfileImageId", "ImageUrl", "ThumbnailUrl", "UserRef" },
                values: new object[] { 3, "https://gillcleerenpluralsight.blob.core.windows.net/files/peachpie.jpg", "https://gillcleerenpluralsight.blob.core.windows.net/files/peachpiesmall.jpg", 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProfileImages",
                keyColumn: "ProfileImageId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProfileImages",
                keyColumn: "ProfileImageId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProfileImages",
                keyColumn: "ProfileImageId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 3);
        }
    }
}
