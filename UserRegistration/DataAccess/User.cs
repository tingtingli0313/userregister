﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public int UserId { get; set; }
        public DateTime DOB { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }

        public virtual ProfileImage ProfileImage { get; set; }
    }
}
