﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _appDbContext;
        public UserRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            var connectionString = _appDbContext.Database.GetDbConnection().ConnectionString;
        }

        public void DeleteUser(int id)
        {
            var deleteUser = _appDbContext.Users.FirstOrDefault(x => x.UserId == id);

            _appDbContext.Remove(deleteUser);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _appDbContext.Users.Include(p => p.ProfileImage);
        }

        public User GetUserById(int id)
        {
            var user = _appDbContext.Users.Where(u => u.UserId == id).Include(x=> x.ProfileImage).SingleOrDefault();
            if (user == null)
            {
                throw new ArgumentNullException("repository", "User not find in the system.");
            }
            return user;
        }

        public IEnumerable<User> GetUsers(int nUsers)
        {
            return _appDbContext.Users.OrderByDescending(u => u.FirstName).Take(nUsers);
        }

        public IEnumerable<User> GetUsersByName(string name)
        {
            return _appDbContext.Users.Where(u => u.FirstName == name || u.LastName == name).Include(x=>x.ProfileImage);
        }

        public IEnumerable<User> SetUpUsers()
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(User user)
        {
            var updateUser = _appDbContext.Users.Where(x => x.UserId == user.UserId).Include(p => p.ProfileImage).SingleOrDefault();
            if (updateUser != null)
            {
                updateUser.FirstName = user.FirstName;
                updateUser.LastName = user.LastName;
                updateUser.Title = user.Title;
                updateUser.ProfileImage.ImageUrl = user.ProfileImage?.ImageUrl;
                updateUser.ProfileImage.ThumbnailUrl = user.ProfileImage?.ThumbnailUrl;

                _appDbContext.SaveChanges();
            }
        }
    }
}
