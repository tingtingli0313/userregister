﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Mvc;
using UserRegistration.Model;
using UserRegistrationBusinessLogic;

namespace UserRegistration.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private  readonly IUserManagementService _userService;
        private readonly IUserRepository _userRepository;

        public UserController(IUserManagementService userManagementService, IUserRepository userRepository)
        {
            
            _userRepository = userRepository;
            _userService = new UserManagementService(_userRepository);

        }

        // GET api/user/users/byNumber/5
        [HttpGet("users/byNumber/{numUsers}")]
        public ActionResult<IEnumerable<UserViewModel>> GetUsers(int numUsers)
        {
            var users= _userService.GetUsers(numUsers);
            var result = new List<UserViewModel>();
            foreach (var user in users)
            {
                result.Add(new UserViewModel
                {
                    Id = user.UserId,
                    ProfileUrl = user.ProfileImage?.ThumbnailUrl,
                    FullName = string.Format("{0} {1}", user.LastName, user.LastName)
                }); 
            }
            return result;
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public ActionResult<UserViewModel> GetUserById(int id)
        {
            var user = _userService.GetUserById(id);
            var userViewModel = new UserViewModel
            {
                Id = user.UserId,
                ProfileUrl = user.ProfileImage?.ImageUrl,
                PhoneNo = user.PhoneNo,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Title = user.Title,
                FullName = string.Format("{0} {1} {2}", user.Title, user.FirstName, user.LastName)
            };

            return userViewModel;
        }
        // GET api/user/users/byName/joe
        [HttpGet("users/byName/{name}")]
        public ActionResult<IEnumerable<UserViewModel>> GetUsers(string name)
        {
            var users = _userService.GetUsersByName(name);
            var result = new List<UserViewModel>();
            foreach (var user in users)
            {
                result.Add(new UserViewModel
                {
                    Id = user.UserId,
                    ProfileUrl = user.ProfileImage?.ThumbnailUrl,
                    FullName = string.Format("{0} {1}", user.LastName, user.LastName)
                });
            }
            return result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]UserViewModel userViewModel)
        {
            var updateUser = new User
            {
                UserId = userViewModel.Id,
                FirstName =  userViewModel.FirstName,
                LastName = userViewModel.LastName,
                Title = userViewModel.Title,
                PhoneNo = userViewModel.PhoneNo,
                Email = userViewModel.Email,
                ProfileImage = new ProfileImage
                    {
                        ImageUrl = userViewModel.ProfileUrl,
                        ThumbnailUrl = userViewModel.ImageUrl,
                        UserRef = userViewModel.Id
                    }
                };
           _userService.UpdateUser(updateUser);
        }

        

        // DELETE api/user/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _userService.DeleteUser(id);
        }
    }
}
