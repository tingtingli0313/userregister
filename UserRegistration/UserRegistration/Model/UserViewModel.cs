﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRegistration.Model
{
    public class UserViewModel
    {
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public DateTime DOB { get; set; }
        public string PhoneNo { get; set; }
        public string ProfileUrl { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }

        public int Id { get; set; }
    }
}
