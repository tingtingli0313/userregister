using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UserRegistrationServiceTests
{
    [TestClass]
    public class UserManagement
    {
        [TestMethod]
        public void CheckTestRun()
        {
            Assert.IsTrue(1-1 == 0);
        }

        [TestMethod]
        public void GetUserById_UserMatches()
        {
            // Arrange
            var user = new User()
            {
                Name = "Test",
            };
            var id = new System.Guid();
            // mock userRepository  
            var mockRepository = new Mock<IUserRepository>();
            mockRepository.Setup(r => r.GetUserById(id)).Returns(user);
            var sut = new UserManagementService(mockRepository.Object);

            // Act
            var result = sut.GetUserById(id);

            // Assert
            Assert.AreEqual(result.Name, user.Name);
        }
    }
}
