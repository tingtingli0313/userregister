﻿using DataAccess;
using System;

namespace UserRegistrationServiceTests
{
    public class UserManagementService : UserRegistrationBusinessLogic.IUserManagementService
    {
        public UserManagementService(IUserRepository repository)
        {
            this.repository = repository;
        }

        public User GetUserById(Guid id)
        {
            var user = repository.GetUserById(id);
            return user;
        }

        public void AddUser(User user)
        {
            repository.AddUser(user);
        }

        private readonly IUserRepository repository;
    }
}
