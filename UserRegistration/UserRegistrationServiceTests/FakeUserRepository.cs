﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserRegistrationServiceTests
{
    public class FakeUserRepository : IUserRepository
    {
        public FakeUserRepository(User user)
        {
            this._user = user;
        }

        public User GetUserById(Guid id)
        {
            _user = new User { Name = "test1"};
            return _user;
        }

        public void AddUser(User user)
        {
            throw new NotImplementedException();
        }

        private User _user;
    }
}
