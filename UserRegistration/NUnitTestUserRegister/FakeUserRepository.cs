﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserRegistrationServiceTests
{
    public class FakeUserRepository : IUserRepository
    {
        private List<User> _allUsers = new List<User>();
        public FakeUserRepository()
        {
            this.SetUpUsers();
        }
        public IEnumerable<User> SetUpUsers()
        {
            this._allUsers = new List<User>
            {
                new User{ FirstName = "test1", LastName="John", UserId = 1,
                          Title="Mr",
                          ProfileImage = new ProfileImage{
                               ThumbnailUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/applepiesmall.jpg",
                               ImageUrl = "https://gillcleerenpluralsight.blob.core.windows.net/files/applepie.jpg"}  },

                new User{ FirstName = "test2", LastName="John", UserId = 2,
                          ProfileImage = new ProfileImage{
                              ThumbnailUrl ="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJU4OPVn48JeOo2gx4_S2EVQH4jJ3qMi6-opxYhXRan28Gpk6T&s",
                              ImageUrl="https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjot8r-3OPmAhUQVH0KHalYB1M4ZBAzKEgwSHoECAEQTw&url=https%3A%2F%2Fwww.whatsup.co.nz%2Fkids%2F&psig=AOvVaw1DgLCoMn5PlxvsLoBNNkBn&ust=1578013626069260&ictx=3&uact=3"},
                          },

                new User{ FirstName = "test3", LastName="John", UserId = 3},

                new User{ FirstName = "test4", LastName="John", UserId = 4},

                new User{ FirstName = "test5", LastName="John", UserId = 5},

                new User{ FirstName = "test6", LastName="John", UserId = 6},

                new User{ FirstName = "test7", LastName="Smith", UserId = 7},

                new User{ FirstName = "test8", LastName="John", UserId = 8},

                new User{ FirstName = "test9", LastName="Smith", UserId = 9},

                new User{ FirstName = "test10", LastName="John", UserId = 10},

                new User{ FirstName = "test11", LastName="John", UserId = 11},

                new User{ FirstName = "test12", LastName="John", UserId = 12},

                new User{ FirstName = "test13", LastName="Smith", UserId = 13},

                new User{ FirstName = "test14", LastName="John", UserId = 14},
            };
            return this._allUsers;
        }

        public User GetUserById(int id)
        {
            //  var user = repository.GetUserById(id);
            var user = _allUsers.Find(x => x.UserId == id);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new ArgumentNullException("repository", "User not exist in the db");
            }
        }

        public IEnumerable<User> GetUsers(int nElement)
        {
            var result = _allUsers.OrderBy(x => x.FirstName).Take(nElement);
            return result;
        }


        public IEnumerable<User> GetUsersByName(string name)
        {
            var result = _allUsers.Where(x => x.FirstName == name || x.LastName == name);

            return result;
        }

        public void DeleteUser(int id)
        {
            var deleteUser = _allUsers.Find(x => x.UserId == id);

            _allUsers.Remove(deleteUser);
        }

        public void AddUser(User user)
        {
            _allUsers.Add(user);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return this._allUsers;
        }

        public void UpdateUser(User user)
        {
            var updateUser = _allUsers.Find(x => x.UserId == user.UserId);
            if (updateUser != null)
            {
                updateUser.FirstName = user.FirstName;
                updateUser.LastName = user.LastName;
                updateUser.Title = user.Title;
                updateUser.PhoneNo = user.PhoneNo;
                updateUser.Email = user.Email;
                updateUser.ProfileImage = new ProfileImage
                {
                    ImageUrl = user.ProfileImage.ImageUrl,
                    ThumbnailUrl = user.ProfileImage.ThumbnailUrl
                };
            }
        }

    }
}
