using DataAccess;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UserRegistrationServiceTests
{
    [TestFixture]
    public class UserManagement
    {
        private Mock<FakeUserRepository> _mockRepository;
        private UserManagementService _userServices;
        private FakeUserRepository testRepo = new FakeUserRepository();
        [SetUp]
        public void Setup()
        {
           _mockRepository = new Mock<FakeUserRepository>();

           _userServices = new UserManagementService(_mockRepository.Object);
        }

        [Test]
        public void CannotCreateUserServiceWithNullUserRepository()
        {
          Assert.Throws<ArgumentNullException> (
             () => {
                 _userServices =  new UserManagementService(null);
             });
        }
        
        [Test]
        public void ThrowExceptionWhenUserIsNotFound()
        {
            //Act
            Assert.Throws<ArgumentNullException>(
            () => {
                var test = _userServices.GetUserById(0);
            });
        }

        [Test]
        public void GetUserById_UserMatches()
        {
            // Arrange
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUserById(1);

            // Assert
            Assert.AreEqual(result.FirstName, "test1");
        }


        [Test]
        public void GetUsers_Top5_UserFinds()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUsers(5).ToList();

            // Assert
            Assert.AreEqual(result.Count, 5);
        }


        [Test]
        public void GetUsers_Top10_UserFinds()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUsers(10).ToList();

            // Assert
            Assert.AreEqual(result.Count, 10);
        }

        [Test]
        public void GetUsersByName_LastName_UserFinds()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUsersByName("Smith").ToList();

            // Assert
            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void GetUsersByName_FirstName_UserFinds()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUsersByName("test1").ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
        }


        [Test]
        public void GetUsersByName_LoadingThumbnails_UserFinds()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var result = sut.GetUsersByName("test1").ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.IsNotNull(result.First().ProfileImage.ThumbnailUrl);
        }

        [Test]
        public void RemoveUserById_RemoveSuccess()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            sut.DeleteUser(10);

            // Assert
            Assert.AreEqual(13, sut.GetAllUsers().Count());

            Assert.Throws<ArgumentNullException>(
              () => {
               var test = sut.GetUserById(10);
           });
        }


        [Test]
        public void UpdateUser_Name_UpdateSuccess()
        {
            var sut = new UserManagementService(_mockRepository.Object);

            // Act
            var user = sut.GetUserById(1);
            user.FirstName = "Debby";
            sut.UpdateUser(user);

            // Assert
            Assert.AreEqual("Debby", user.FirstName);
        }

    }
}
