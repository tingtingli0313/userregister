﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using UserRegistrationBusinessLogic;

namespace UserRegistrationServiceTests
{
    public class UserManagementService : IUserManagementService
    {

        public UserManagementService(IUserRepository repository)
        {
            if(repository == null)
            {
                throw new ArgumentNullException("repository", "A valid user repository must be supplied.");
            }
            this.repository = repository;
            this.repository.SetUpUsers();
        }

        public IEnumerable<User> SetUpUsers()
        {
            return this.repository.SetUpUsers();
        }

        public User GetUserById(int id)
        {
            return this.repository.GetUserById(id);
        }

        public IEnumerable<User> GetUsers(int nElement)
        {
            return this.repository.GetUsers(nElement);
        }


        public IEnumerable<User> GetUsersByName(string name)
        {
            return repository.GetUsersByName(name);
        }

        public void DeleteUser(int id)
        {
            repository.DeleteUser(id);
        }

        //public void AddUser(User user)
        //{
        //    repository.AddUser(user);
        //}

        public IEnumerable<User> GetAllUsers()
        {
            return repository.GetAllUsers();
        }

        public void UpdateUser(User user)
        {
            repository.UpdateUser(user);
        }


        private readonly IUserRepository repository;
    }
}
