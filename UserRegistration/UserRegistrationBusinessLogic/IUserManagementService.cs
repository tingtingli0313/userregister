﻿using DataAccess;
using System.Collections.Generic;

namespace UserRegistrationBusinessLogic
{
    public interface IUserManagementService
    {
        User GetUserById(int id);
        void DeleteUser(int id);
        void UpdateUser(User user);

        IEnumerable<User> GetUsersByName(string name);
        IEnumerable<User> GetUsers(int nUsers);
        IEnumerable<User> GetAllUsers();
        IEnumerable<User> SetUpUsers();
    }
}
