﻿using System;
using System.Collections.Generic;
using DataAccess;

namespace UserRegistrationBusinessLogic
{
    public class UserManagementService : IUserManagementService
    {
        private IUserRepository _userRepository;

        public UserManagementService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public void AddUser(User user)
        {
            this.UpdateUser(user);
        }

        public void DeleteUser(int id)
        {
            _userRepository.DeleteUser(id);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public User GetUserById(int id)
        {
            var user = _userRepository.GetUserById(id);
            return user;
        }


        public IEnumerable<User> GetUsers(int nUsers)
        {
            return _userRepository.GetUsers(nUsers);
        }

        public IEnumerable<User> GetUsersByName(string name)
        {
            return _userRepository.GetUsersByName(name);
        }

        public IEnumerable<User> SetUpUsers()
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(User user)
        {
            _userRepository.UpdateUser(user);
        }
    }
}
